using System;

namespace ersatz.Models
{
    public class TranslateResponse
    {
        public string Message { get; set; }
        public string Language { get; set; }
    }
}