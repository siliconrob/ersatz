using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ersatz.Models;
using Microsoft.AspNetCore.Mvc;

namespace ersatz.Controllers
{
    [Route("api/[controller]")]
    public class TranslateController : Controller
    {
        [HttpGet]
        public async Task<IActionResult> Ping(string text)
        {
            await Task.Delay(TimeSpan.FromSeconds(1));

            var req = new
            {
                Request = text
            };
            var result = req.GetOrSet(() => TranslateResponses(req.Request));
            return Ok(result);
        }

        private static List<TranslateResponse> TranslateResponses(string text)
        {
            var result = new List<TranslateResponse>
            {
                new TranslateResponse
                {
                    Message = (text ?? "Pong").Trim(),
                }
            };
            return result;
        }
    }
}