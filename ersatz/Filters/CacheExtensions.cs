using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Newtonsoft.Json;
using StackExchange.Redis;

namespace ersatz
{
    public static class CacheExtensions
    {
        public static string RedisConnectionString { get; set; } = "localhost";

        private static byte[] GetMD5(this byte[] data)
        {
            using (var hashString = new MD5CryptoServiceProvider())
            {
                return hashString.ComputeHash(data);
            }
        }
        
        public static string MD5(this string text)
        {
            if (text == null)
            {
                return string.Empty;
            }
            var result = new Guid(Encoding.ASCII.GetBytes(text).GetMD5().Aggregate(string.Empty, (current, x) => $"{current}{x:x2}"));
            return result.ToString();
        }

        public static Func<object, string> RedisKeyFn { get; set; }

        public static string DefaultRedisKey(this object input)
        {
            input = input ?? new object();
            return JsonConvert.SerializeObject(input).MD5();
        }

        public static T GetOrSet<T>(this object input, Func<T> getFn, string key = null, TimeSpan? expiresIn = null) where T : class
        {
            var redis = ConnectionMultiplexer.Connect(RedisConnectionString);
            var rkey = key ?? DefaultRedisKey(input);
            var db = redis.GetDatabase();
            var result = db.StringGet(rkey);
            if (result != RedisValue.Null)
            {
                return JsonConvert.DeserializeObject<T>(result.ToString());
            }
            getFn = getFn ?? (() => default(T));
            var data = getFn();
            if (!db.StringSet(rkey, JsonConvert.SerializeObject(data), expiresIn ?? TimeSpan.FromSeconds(5)))
            {
                throw new ApplicationException("Redis failure to save key/value");
            }
            return data;
        }
    }
}