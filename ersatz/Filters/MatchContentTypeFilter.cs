using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Net.Http.Headers;

namespace ersatz.Filters
{
    public class MatchContentTypeFilter : IResourceFilter
    {
        public void OnResourceExecuting(ResourceExecutingContext context)
        {
            var headers = context.HttpContext.Request.Headers;
            var requestResponseContents = new
            {
                ContentType = headers.TryGetValue(HeaderNames.ContentType),
                Accept =headers.TryGetValue(HeaderNames.Accept)
            };
            if (string.IsNullOrEmpty(requestResponseContents.ContentType) || requestResponseContents.ContentType.Equals(requestResponseContents.Accept))
            {
                return;
            }
            if (!headers.ContainsKey(HeaderNames.Accept))
            {
                return;
            }
            headers.Remove(HeaderNames.Accept);
            headers.Add(HeaderNames.Accept, requestResponseContents.ContentType);
        }

        public void OnResourceExecuted(ResourceExecutedContext context)
        {
        }
    }
}