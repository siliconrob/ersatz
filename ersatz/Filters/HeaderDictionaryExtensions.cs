using Microsoft.AspNetCore.Http;

namespace ersatz.Filters
{
    internal static class HeaderDictionaryExtensions
    {
        public static string TryGetValue(this IHeaderDictionary dict, string key)
        {
            if (dict.TryGetValue(key, out var keyValue))
            {
                return keyValue;
            }
            return "";
        }
    }    
}